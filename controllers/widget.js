// region Variables

var args = null;
var leftButtonFunction = null;
var leftButtonFunctionSingleUse = null;
var rightButtonFunction = null;
var rightMenuImage = null;
var leftMenuProperties = {
    closeButtonIcon: '',
    backButtonIcon: '',
    itemDefaultBackgroundColor: 'white',
    itemActiveBackgroundColor: 'red',
    itemHeight: Ti.UI.SIZE,
    itemIconLeft: 20,
    itemIconHeight: 20,
    itemFontColor: 'black',
    itemActiveFontColor: 'black',
    itemFontSize: 12,
    itemFontFamily: 'OpenSans',
    menuIcon: '',
    menuIconLeft: 20,
    notificationBackgroundColor: 'blue',
    notificationFontColor: 'white',
    notificationFontSize: 12,
    notificationHeight: 25,
    notificationRight: 20,
    notificationBorderRadius: 3,
    rowSeparatorColor: 'transparent'
};
var previousViewContentIndex = null;
var menuAnimationSpeed = 500;
var screenSize = {
    height: 0,
    width: 0
};
var menuItems = [];
var viewChanging = false;

// endregion

// region Functions

function init() {

    args = $.args;

    if (args) {

        // Get Menu Properties
        leftMenuProperties.backButtonIcon = args.backButtonIcon || WPATH('/images/menu_icon_back.png');
        leftMenuProperties.closeButtonIcon = args.closeButtonIcon || WPATH('/images/menu_icon_close.png');
        leftMenuProperties.itemDefaultBackgroundColor = args.itemDefaultBackgroundColor || 'white';
        leftMenuProperties.itemActiveBackgroundColor = args.itemActiveBackgroundColor || 'white';
        leftMenuProperties.itemHeight = args.itemHeight || Ti.UI.SIZE;
        leftMenuProperties.itemIconLeft = args.itemIconLeft || 20;
        leftMenuProperties.itemIconHeight = args.itemIconHeight || 20;
        leftMenuProperties.itemFontColor = args.itemFontColor || 'white';
        leftMenuProperties.itemActiveFontColor = args.itemActiveFontColor || 'black';
        leftMenuProperties.itemFontSize = args.itemFontSize || 12;
        leftMenuProperties.itemFontFamily = args.itemFontFamily || 'OpenSans';
        leftMenuProperties.menuIcon = args.leftMenuButtonIcon || WPATH('/images/menu_icon.png');
        leftMenuProperties.menuIconLeft = args.leftMenuIconLeft || 20;
        leftMenuProperties.notificationBackgroundColor = args.notificationBackgroundColor || 'blue';
        leftMenuProperties.notificationFontColor = args.notificationFontColor || 'white';
        leftMenuProperties.notificationFontSize = args.notificationFontSize || 12;
        leftMenuProperties.notificationHeight = args.notificationHeight || 25;
        leftMenuProperties.notificationRight = args.notificationRight || 10;
        leftMenuProperties.notificationBorderRadius = args.notificationBorderRadius || 3;
        leftMenuProperties.notificationBorderColor = args.notificationBorderColor || 'transparent';
        leftMenuProperties.rowSeparatorColor = args.rowSeparatorColor || 'transparent';
        rightMenuImage = args.rightMenuButtonIcon || WPATH('/images/menu_icon_action.png');

        // Get Screen Height And Width
        $.menuIconContainerLeft.buttonType = 'menu';

        // Set General Properties
        $.leftMenuItems.setBackgroundColor(args.backgroundColor || 'white');
        $.leftMenuContainer.setBackgroundColor(args.backgroundColor || 'white');
        $.navigationBarWrapper.setBackgroundColor(args.navigationBarBackgroundColor || 'white');
        $.statusBar.setBackgroundColor(args.statusBarBackgroundColor || 'white');
        $.menuIconLeft.setImage(leftMenuProperties.menuIcon);
        $.menuIconLeft.setHeight(args.leftMenuButtonHeight || 20);
        $.menuIconLeft.setLeft(args.leftMenuIconLeft || 20);
        $.menuIconRight.setImage(args.rightMenuButtonIcon || WPATH('/images/menu_icon_action.png'));
        $.menuIconRight.setHeight(args.rightMenuButtonHeight || 20);
        $.navigationBarWrapper.setHeight(args.navigationBarHeight || 60);
        $.contentViewerItems.setTop(args.navigationBarHeight || 60);
        $.contentViewerMask.setTop(args.navigationBarHeight || 60);
        $.rightMenuContainer.setTop(args.navigationBarHeight || 60);
        $.viewTitle.setColor(args.viewTitleColor || 'white');
        $.viewTitle.setFont({
            fontSize: args.viewTitleFontSize || 12,
            fontFamily: args.viewTitleFontFamily || 'OpenSans-Regular'
        });
        menuAnimationSpeed = args.menuAnimationSpeed || 500;
        $.contentViewerMask.setZIndex(100);
        $.rightMenuContainer.setZIndex(101);

        // Get Menu Elements
        if (args.children) {
            _.each(args.children, function (argument) {
                if (argument.role) {

                    // Add Left Menu Header
                    if (argument.role == 'menuHeader') {
                        var header = argument;
                        header.setHeight(header.height || 100);
                        header.setTop(header.top || 0);
                        $.leftMenuScrollWrapper.setTop((header.height || 100) + (header.top || 0));
                        $.leftMenuContainer.add(header);
                    }

                    // Get Menu Items
                    if (argument.role == 'menuItem') {
                        var menuItem = {
                            viewTitle: argument.menuItemTitle,
                            defaultIcon: argument.defaultIcon,
                            activeIcon: argument.activeIcon,
                            iconHeight: argument.iconHeight || 20,
                            top: argument.top || 0,
                            headerView: null,
                            contentView: null,
                            rightMenuItems: []
                        };

                        _.each(argument.getChildren(), function (menuComponent) {

                            // Get Navigation Bar Header View
                            if (menuComponent.role == 'navigationBarHeader') {
                                menuItem.headerView = menuComponent;
                            }

                            // Get View Content And View Right Menu Items
                            if (menuComponent.role == 'content') {
                                var contentView = menuComponent;

                                // Get Right Menu Items
                                _.each(contentView.getChildren(), function (contentItem) {
                                    _.each(contentItem.getChildren(), function (contentSubItem) {
                                        if (contentSubItem.role == 'rightMenuItems') {
                                            if (contentSubItem.getChildren()) {
                                                _.each(contentSubItem.getChildren(), function (itemGroups) {
                                                    if (itemGroups && itemGroups.role == 'itemGroups') {
                                                        var sections = itemGroups.getSections();
                                                        if (sections) {
                                                            _.each(sections, function (section) {
                                                                menuItem.rightMenuItems.push(section);
                                                            });
                                                        }
                                                    }
                                                });
                                            }

                                            contentItem.remove(contentSubItem);
                                        }
                                    });
                                });

                                // Get View Content
                                contentView.visible = false;
                                $.contentViewerItems.add(contentView);
                                contentView = null;
                                argument.remove(menuComponent);
                            }

                        });

                        menuItems.push(menuItem);
                    }

                    // Add Left Menu Footer
                    if (argument.role == 'menuFooter') {
                        var footer = argument;
                        footer.setHeight(footer.height || 100);
                        footer.setBottom(footer.bottom || 0);
                        $.leftMenuScrollWrapper.setBottom((footer.height || 100) + (footer.bottom || 0));
                        $.leftMenuContainer.add(footer);
                    }

                }
            });
        }

        // Add Menu Elements
        if (menuItems && menuItems.length > 0) {

            // Add Menu Item Index
            menuItems = _.map(menuItems, function (menuItem, index) {
                menuItem.menuItemIndex = index;
                return menuItem;
            });

            // Add Left Menu Items
            $.leftMenuItemListSection.setItems(_.map(menuItems, function (menuItem, index) {
                return {
                    menuItemIndex: index,
                    notificationWrapper: {
                        backgroundColor: leftMenuProperties.notificationBackgroundColor,
                        height: leftMenuProperties.notificationHeight,
                        right: leftMenuProperties.notificationRight,
                        borderRadius: leftMenuProperties.notificationBorderRadius,
                        borderColor: leftMenuProperties.notificationBorderColor
                    },
                    notificationCount: {
                        color: leftMenuProperties.notificationFontColor,
                        font: {
                            fontSize: leftMenuProperties.notificationFontSize
                        }
                    },
                    leftMenuItemContainer: {
                        top: menuItem.top,
                        height: leftMenuProperties.itemHeight,
                        backgroundColor: leftMenuProperties.itemDefaultBackgroundColor
                    },
                    rowSeparatorColor: {
                        backgroundColor: leftMenuProperties.rowSeparatorColor
                    },
                    itemIcon: {
                        image: menuItem.defaultIcon || menuItem.activeIcon,
                        left: leftMenuProperties.itemIconLeft,
                        height: leftMenuProperties.itemIconHeight,
                        defaultIcon: menuItem.defaultIcon,
                        activeIcon: menuItem.activeIcon
                    },
                    itemLabel: {
                        text: menuItem.viewTitle,
                        left: leftMenuProperties.itemIconHeight + (leftMenuProperties.itemIconLeft * 2),
                        color: leftMenuProperties.itemFontColor,
                        font: {
                            fontSize: leftMenuProperties.itemFontSize,
                            fontFamily: leftMenuProperties.itemFontFamily
                        }
                    },
                    template: 'leftMenuItemTemplate'
                };
            }));

        }

        // Add Event Listeners
        $.contentViewerItems.addEventListener('viewOpen', function (e) {
            $.trigger('viewOpen', e);
        });
        $.contentViewerItems.addEventListener('viewClosed', function (e) {
            $.trigger('viewClosed', e);
        });
        $.contentViewerItems.addEventListener('setMenuItemOptions', function (e) {
            $.trigger('setMenuItemOptions', e);
        });

        // Set Default View
        setActiveView(0);

    }

}
init();

// Close Right Menu
function closeRightMenu() {

    if ($.rightMenuContainer.isOpen == true) {
        $.rightMenuContainer.setVisible(false);
        $.contentViewerMask.setVisible(false);
        $.rightMenuContainer.isOpen = false;
    }

}

// Get Content Viewer Size
function getContentViewerSize(e) {

    if (e.source && e.source.size) {
        screenSize.width = e.source.size.width;
        screenSize.height = e.source.size.height;
    }

}

// Hide Left Menu
function hideLeftMenu() {

    if ($.rightMenuContainer.isOpen) {
        showRightMenu();
    } else {
        showLeftMenu();
    }

}

// Select Left Menu Item
function selectLeftMenuItem(e) {

    // Get Selected Item
    var itemIndex = e.itemIndex;
    var item = e.section.getItemAt(itemIndex);

    // Set Active View
    setActiveView(item.menuItemIndex);

    // Close Menu
    showLeftMenu();

}

// Set Active Content View
function setActiveView(contentViewIndex) {

    if (!viewChanging) {

        viewChanging = true;

        // Remove Custom Views
        _.each($.contentViewerItems.getChildren(), function (contentView, index) {
            if (index >= menuItems.length) {
                contentView.fireEvent('customViewClosed');
                $.contentViewerItems.remove(contentView);
            }
        });

        // Reset Menu Icon
        if ($.menuIconContainerLeft.buttonType != 'menu') {
            leftButtonFunction = null;
            leftButtonFunctionSingleUse = null;
            $.menuIconLeft.setOpacity(0);
            $.menuIconLeft.setImage(leftMenuProperties.menuIcon);
            $.menuIconLeft.animate({
                opacity: 1,
                duration: menuAnimationSpeed
            });
            $.menuIconContainerLeft.buttonType = 'menu';
        }

        // Set View
        if (contentViewIndex || contentViewIndex === 0) {

            // Variables
            var currentMenuItem = menuItems[contentViewIndex];

            // Set Active Content View Elements
            if (currentMenuItem) {

                // Highlight Selected Menu Item
                if ($.leftMenuItemListSection.getItems()) {

                    // Deselect Previous Menu Item
                    if (previousViewContentIndex != null) {
                        var previousMenuItem = $.leftMenuItemListSection.getItems()[previousViewContentIndex];
                        previousMenuItem.leftMenuItemContainer.backgroundColor = leftMenuProperties.itemDefaultBackgroundColor;
                        previousMenuItem.itemIcon.image = previousMenuItem.itemIcon.defaultIcon;
                        previousMenuItem.itemLabel.color = leftMenuProperties.itemFontColor;
                        $.leftMenuItemListSection.updateItemAt(previousViewContentIndex, previousMenuItem);
                    }

                    // Select New Menu Item
                    var newMenuItem = $.leftMenuItemListSection.getItems()[contentViewIndex];
                    newMenuItem.leftMenuItemContainer.backgroundColor = leftMenuProperties.itemActiveBackgroundColor;
                    newMenuItem.itemIcon.image = newMenuItem.itemIcon.activeIcon;
                    newMenuItem.itemLabel.color = leftMenuProperties.itemActiveFontColor;
                    $.leftMenuItemListSection.updateItemAt(contentViewIndex, newMenuItem);

                }

                // Set Navigation Bar View/Title
                if (currentMenuItem.headerView) {
                    setViewHeader(currentMenuItem.headerView);
                }
                else {
                    setViewTitle(currentMenuItem.viewTitle);
                }

                // Show New Content View
                if (!isNaN(contentViewIndex)) {
                    $.contentViewerItems.getChildren()[contentViewIndex].setOpacity(0);
                    $.contentViewerItems.getChildren()[contentViewIndex].setVisible(true);
                    $.contentViewerItems.getChildren()[contentViewIndex].animate({
                        opacity: 1,
                        duration: menuAnimationSpeed
                    }, function () {
                        viewChanging = false;
                    });

                    // Hide Previous Content View
                    if ((previousViewContentIndex || previousViewContentIndex == 0) && previousViewContentIndex != contentViewIndex) {
                        $.contentViewerItems.getChildren()[previousViewContentIndex].setOpacity(1);
                        $.contentViewerItems.getChildren()[previousViewContentIndex].setVisible(false);
                        $.contentViewerItems.getChildren()[previousViewContentIndex].animate({
                            opacity: 0,
                            duration: menuAnimationSpeed
                        });
                        rightButtonFunction = null;
                        $.menuIconRight.setImage(rightMenuImage);

                        // Fire Close Event On Previous Item
                        $.contentViewerItems.fireEvent('viewClosed', menuItems[previousViewContentIndex]);
                    }
                    previousViewContentIndex = contentViewIndex;
                }

                // Set Right Menu Items
                $.rightMenuItems.setData([]);
                if (currentMenuItem.rightMenuItems && currentMenuItem.rightMenuItems.length > 0) {
                    $.rightMenuItems.setData(currentMenuItem.rightMenuItems);
                    $.menuIconContainerRight.setVisible(true);
                    $.menuIconRight.setVisible(true);
                } else if (rightButtonFunction) {
                    $.menuIconContainerRight.setVisible(true);
                    $.menuIconRight.setVisible(true);
                }
                else {
                    $.menuIconContainerRight.setVisible(false);
                    $.menuIconRight.setVisible(false);
                }

                // Fire Open Event
                $.contentViewerItems.fireEvent('viewOpen', currentMenuItem);

            }

        }

    }

}

// Set View Header
function setViewHeader(headerView) {

    if (headerView) {
        _.each($.viewTitleContainer.getChildren(), function (navItem) {
            if (navItem.id != 'viewTitle') {
                $.viewTitleContainer.remove(navItem);
            }
        });
        $.viewTitle.setVisible(false);
        headerView.setOpacity(0);
        headerView.setVisible(true);
        $.viewTitleContainer.add(headerView);
        headerView.animate({
            opacity: 1,
            duration: menuAnimationSpeed
        });
    }

}

// Set View Title
function setViewTitle(title) {

    _.each($.viewTitleContainer.getChildren(), function (navItem) {
        if (navItem.id != 'viewTitle') {
            $.viewTitleContainer.remove(navItem);
        }
    });
    $.viewTitle.setText(title);
    $.viewTitle.setOpacity(0);
    $.viewTitle.setVisible(true);
    $.viewTitle.animate({
        opacity: 1,
        duration: menuAnimationSpeed
    });

}

// Show Left Menu
function showLeftMenu() {

    // Check For Custom Menu Button Function
    if (leftButtonFunctionSingleUse) {
        leftButtonFunctionSingleUse();
        leftButtonFunctionSingleUse = null;
        return;
    }
    if (leftButtonFunction) {
        leftButtonFunction();
        return;
    }

    // Variables
    var totalViews = $.contentViewerItems.getChildren().length;
    var customView = $.contentViewerItems.getChildren()[totalViews - 1];

    // Handle Custom View
    if (customView) {

        // Go Back 1 View
        if (customView.buttonType == 'back') {
            customView.animate({
                left: screenSize.width,
                opacity: 0,
                duration: menuAnimationSpeed
            }, function () {
                customView.fireEvent('customViewClosed');
                $.contentViewerItems.remove(customView);

                // Change Menu Back To Menu Icon
                var totalCurrentViews = $.contentViewerItems.getChildren().length;
                if (totalCurrentViews == menuItems.length) {
                    $.menuIconLeft.setOpacity(0);
                    $.menuIconLeft.setImage(leftMenuProperties.menuIcon);
                    $.menuIconLeft.animate({
                        opacity: 1,
                        duration: menuAnimationSpeed - (menuAnimationSpeed * .5)
                    });
                    $.menuIconContainerLeft.buttonType = 'menu';

                    // Set Navigation Bar View/Title
                    var currentMenuItem = menuItems[previousViewContentIndex];
                    if (currentMenuItem) {
                        if (currentMenuItem.headerView) {
                            setViewHeader(currentMenuItem.headerView);
                        } else if (currentMenuItem.viewTitle) {
                            setViewTitle(currentMenuItem.viewTitle);
                        }

                        // Fire Open Event
                        $.contentViewerItems.fireEvent('viewOpen', currentMenuItem);
                    }
                } else {
                    // Set View Title
                    var previousView = $.contentViewerItems.getChildren()[totalViews - 2];
                    if (previousView) {
                        if (previousView.viewTitle) {
                            setViewTitle(previousView.viewTitle);
                        }

                        // Fire Open Event
                        previousView.fireEvent('customViewOpened', {});

                        // Set Menu Button Back To Menu Icon
                        if (previousView.buttonType != 'back') {
                            if ($.menuIconContainerLeft.buttonType != 'menu') {
                                $.menuIconLeft.setOpacity(0);
                                $.menuIconLeft.setImage(leftMenuProperties.menuIcon);
                                $.menuIconLeft.animate({
                                    opacity: 1,
                                    duration: menuAnimationSpeed - (menuAnimationSpeed * .5)
                                });
                            }
                        }
                    }
                }
            });

            return;
        }

        // Close Custom View
        else if (customView.buttonType == 'close') {

            // Remove Custom Views
            _.each($.contentViewerItems.getChildren(), function (contentView, index) {
                if (index >= menuItems.length) {
                    customView.fireEvent('customViewClosed');
                    $.contentViewerItems.remove(contentView);
                }
            });

            // Set View Header/Title
            if (previousViewContentIndex || previousViewContentIndex == 0) {
                var currentMenuItem = menuItems[previousViewContentIndex];
                if (currentMenuItem.headerView) {
                    setViewHeader(currentMenuItem.headerView);
                } else {
                    setViewTitle(currentMenuItem.viewTitle);
                }
            }

            // Set Menu Icon
            $.menuIconLeft.setOpacity(0);
            $.menuIconLeft.setImage(leftMenuProperties.menuIcon);
            $.menuIconLeft.animate({
                opacity: 1,
                duration: menuAnimationSpeed
            });
            $.menuIconContainerLeft.buttonType = 'menu';
            return;

        }

    }

    // Remove Custom Views
    _.each($.contentViewerItems.getChildren(), function (contentView, index) {
        if (index >= menuItems.length) {
            $.contentViewerItems.remove(contentView);
        }
    });

    // Reset Menu Icon
    if ($.menuIconContainerLeft.buttonType != 'menu') {
        $.menuIconLeft.setOpacity(0);
        $.menuIconLeft.setImage(leftMenuProperties.menuIcon);
        $.menuIconLeft.animate({
            opacity: 1,
            duration: menuAnimationSpeed - (menuAnimationSpeed * .5)
        });
    }

    // Close Left Menu
    if ($.leftMenuContainer.isOpen == true) {
        // Hide Menu Viewer Mask
        $.contentViewerMask.setVisible(false);

        // Shift Content Viewer Back To Original Location
        $.contentViewerContainer.animate({
            left: 0,
            right: 0,
            duration: menuAnimationSpeed
        });

        // Hide Menu Items
        $.leftMenuContainer.animate({
            left: screenSize.width * .8,
            duration: menuAnimationSpeed
        });

        //Set Menu Open Status To Closed
        $.leftMenuContainer.isOpen = false;
    }

    // Open Left Menu
    else {
        // Show Content Viewer Mask
        $.contentViewerMask.setVisible(true);

        // Show Menu Items
        $.leftMenuContainer.animate({
            left: 0,
            duration: menuAnimationSpeed
        });

        // Shift Content Viewer Right
        var newRightPosition = -1 * (screenSize.width * .8);
        var newLeftPosition = screenSize.width * .8;
        $.contentViewerContainer.animate({
            left: newLeftPosition,
            right: newRightPosition,
            duration: menuAnimationSpeed
        });

        // Hide Right Menu
        $.rightMenuContainer.setVisible(false);
        $.rightMenuContainer.isOpen = false;

        // Set Menu Open Status To Open
        $.leftMenuContainer.isOpen = true;
    }

}

// Show Right Menu
function showRightMenu() {

    if (rightButtonFunction) {
        rightButtonFunction();
    } else {
        // Close Right Menu
        if ($.rightMenuContainer.isOpen == true) {
            $.rightMenuContainer.setVisible(false);
            $.contentViewerMask.setVisible(false);
            $.rightMenuContainer.isOpen = false;
        }
        // Open Right Menu
        else {
            $.rightMenuContainer.setOpacity(0);
            $.rightMenuContainer.setVisible(true);
            $.rightMenuContainer.animate({
                opacity: 1,
                duration: 150
            });
            $.contentViewerMask.setVisible(true);
            $.rightMenuContainer.isOpen = true;
        }
    }

}

// endregion

// region Exports

exports = {

    // Close Current View
    closeCurrentView: function () {

        if (menuItems) {
            var menuItemsCount = menuItems.length;
            var contentViewerItemsCount = $.contentViewerItems.getChildren().length;

            if (contentViewerItemsCount - 1 >= menuItemsCount) {
                $.contentViewerItems.remove($.contentViewerItems.getChildren()[contentViewerItemsCount - 1]);
            }
        }

    },

    // Close Right Menu
    closeRightMenu: closeRightMenu,

    // Show Menu Icon
    showMenuIcon: function (show) {

        if (show) {
            $.menuIconLeft.setVisible(true);
        } else {
            $.menuIconLeft.setVisible(false);
        }

    },

    // Set Active View
    setActiveView: setActiveView,

    // Set Navigation Bar Background Color
    setNavigationBarBackgroundColor: function (color) {

        $.navigationBarWrapper.setBackgroundColor(color);

    },

    // Set Notification Count
    setNotificationCount: function (menuItemIndex, count) {

        if (menuItemIndex) {
            var menuItem = $.leftMenuItemListSection.getItems()[menuItemIndex];

            if (menuItem) {
                if (count) {
                    menuItem.notificationCount.text = count;
                    menuItem.notificationWrapper.visible = true;
                } else {
                    menuItem.notificationWrapper.visible = false;
                }

                $.leftMenuItemListSection.updateItemAt(menuItemIndex, menuItem);
            }
        }

    },

    // Set Left Button Function
    setLeftButtonFunction: function (callBack) {

        leftButtonFunction = callBack;

    },

    // Set Left Button Function For Single Use
    setLeftButtonFunctionSingleUse: function (callBack) {

        leftButtonFunctionSingleUse = callBack;

    },

    // Set Left Button Image
    setLeftButtonImage: function (imagePath) {

        if (imagePath) {
            $.menuIconLeft.setOpacity(0);
            $.menuIconLeft.setImage(imagePath);
            $.menuIconLeft.animate({
                opacity: 1,
                duration: menuAnimationSpeed
            });
        }

    },

    // Set Left Button Type
    setLeftButtonType: function (buttonType) {

        if (buttonType && $.menuIconContainerLeft.buttonType != buttonType) {
            $.menuIconLeft.setVisible(true);
            $.menuIconLeft.setOpacity(0);
            switch (buttonType) {
                case 'back':
                    $.menuIconLeft.setImage(leftMenuProperties.backButtonIcon);
                    break;
                case 'close':
                    $.menuIconLeft.setImage(leftMenuProperties.closeButtonIcon);
                    break;
                case 'menu':
                    $.menuIconLeft.setImage(leftMenuProperties.menuIcon);
                    break;
            }

            $.menuIconLeft.animate({
                opacity: 1,
                duration: menuAnimationSpeed
            });
            $.menuIconContainerLeft.buttonType = buttonType;
        }

    },

    // Set Menu Item Options
    setMenuItemOptions: function (contentViewIndex, options) {

        var currentMenuItem = menuItems[contentViewIndex];
        var _options = {
            currentMenuItem: currentMenuItem,
            options: options
        };
        $.contentViewerItems.fireEvent('setMenuItemOptions', _options);

    },

    // Set Right Button Function
    setRightButtonFunction: function (callBack, iconPath, buttonText) {

        rightButtonFunction = callBack;

        if (callBack) {
            $.menuIconContainerRight.setVisible(true);
            $.menuIconRight.setVisible(true);

            if (iconPath) {
                $.menuIconRight.setImage(iconPath);
            } else {
                if (buttonText) {
                    $.menuIconRight.setVisible(false);
                    $.rightMenuButtonText.setText(buttonText.text);
                    $.rightMenuButtonText.setColor(buttonText.color);
                    $.rightMenuButtonText.setFont(buttonText.font);
                } else {
                    $.menuIconRight.setVisible(false);
                    $.rightMenuButtonText.setVisible(false);
                }
            }
        }
        else {
            $.menuIconContainerRight.setVisible(false);
            $.menuIconRight.setVisible(false);
        }

    },

    // Set View Header
    setViewHeader: setViewHeader,

    // Set View Title
    setViewTitle: setViewTitle,

    // Show Custom View
    showCustomView: function (customView, buttonType, viewTitle, viewHeader, options) {

        if (!viewChanging) {
            viewChanging = true;

            // Set Menu Button Type
            if (buttonType && $.menuIconContainerLeft.buttonType != buttonType) {
                $.menuIconLeft.setOpacity(0);
                switch (buttonType) {
                    case 'back':
                        $.menuIconLeft.setImage(leftMenuProperties.backButtonIcon);
                        break;
                    case 'close':
                        $.menuIconLeft.setImage(leftMenuProperties.closeButtonIcon);
                        break;
                    case 'menu':
                        $.menuIconLeft.setImage(leftMenuProperties.menuIcon);
                        break;
                }

                $.menuIconLeft.animate({
                    opacity: 1,
                    duration: menuAnimationSpeed
                });
                $.menuIconContainerLeft.buttonType = buttonType;
            }

            // Set View Options
            if (options) {
                if (typeof customView.setOptions === 'function') {
                    customView.setOptions(options);
                }
            }

            // Call Custom View Opened Function
            if (typeof customView.opened === 'function') {
                customView.opened();
            }

            // Add Custom View Events
            var viewToShow = customView.getView();
            viewToShow.addEventListener('customViewOpened', function () {
                if (typeof customView.opened === 'function') {
                    customView.opened();
                }
            });
            viewToShow.addEventListener('customViewClosed', function () {
                if (typeof customView.closed === 'function') {
                    customView.closed();
                }
            });

            // Set View Title
            if (viewHeader) {
                setViewHeader(viewHeader);
            } else if (viewTitle) {
                viewToShow.viewTitle = viewTitle;
                setViewTitle(viewTitle);
            }

            // Add Custom View To Content Viewer
            viewToShow.buttonType = buttonType;
            viewToShow.setOpacity(0);
            viewToShow.setWidth(screenSize.width);
            viewToShow.setLeft(screenSize.width - 1);
            $.contentViewerItems.add(viewToShow);

            // Animate View
            viewToShow.animate({
                opacity: 1,
                left: 0,
                duration: menuAnimationSpeed
            }, function () {
                viewChanging = false;
            });
        }

    }

};

// endregion
